package homeworks.arrays;

import java.util.Objects;
import java.util.Random;

/* Write Computer class, the attributes of this class is manufacturer (of String type),
serialNumber (of int type), price (of float type),
quantityCPU (of int type) and frequencyCPU (of int type).
The fields Computer class need to be encapsulated.
Add to Computer class getters and setters methods. Use correct access modifiers.
Write a program to create array of Computer objects (5 pcs.).
Declare array of Computer objects (5 pcs.), create 5 Computer objects and place it to array.
Write a program that iterate through array of Computer objects and increases by 10 percent field price.
Add to class Computer method void view(){} that prints all fields of object in line.
Print all info (fields) of all objects in console.*/
public class Computer {
    private String manufacturer;
    private float price;
    private int quantityCPU;
    private int frequencyCPU;//move to constant

    public static final Random RANDOM = new Random();

    public Computer(int lowestPrice, int highestPrice) {
        this.price = RANDOM.nextFloat() * (highestPrice - lowestPrice + 1) + lowestPrice;
        this.quantityCPU = RANDOM.nextInt(10) + 1;
        this.frequencyCPU = RANDOM.nextInt(10) + 1;

        StringBuilder sb = new StringBuilder();

        sb.append((char) (RANDOM.nextInt(26) + 65));

//        System.gc();

        for (int i = 0; i < RANDOM.nextInt(10); i++) {

            sb.append((char) (RANDOM.nextInt(26) + 97));
        }

        this.manufacturer = sb.toString();
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        if (Objects.isNull(manufacturer)) {
            System.out.println("Wrong manufacturer!");
            return;
        }

        this.manufacturer = manufacturer;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        if (price < 0) {
            System.out.println("Wrong price!");
            return;
        }

        this.price = price;
    }

    public int getQuantityCPU() {
        return quantityCPU;
    }

    public void setQuantityCPU(int quantityCPU) {
        if (quantityCPU < 0) {
            System.out.println("Wrong quantity CPU!");
        }

        this.quantityCPU = quantityCPU;
    }

    public int getFrequencyCPU() {
        return frequencyCPU;
    }

    public void setFrequencyCPU(int frequencyCPU) {
        if (frequencyCPU < 0) {
            System.out.println("Wrong frequency CPU!");
        } else
            this.frequencyCPU = frequencyCPU;
    }

    public void view() {
        float xTenPrice = price * 10;

        price = (float) Math.round(xTenPrice) / 10;

        System.out.println("Manufacturer: " + manufacturer + "\nPrice: " +
                price + "\nQuantity CPU: " + quantityCPU + "\tFrequency CPU: " + frequencyCPU + "\n");
    }

    public void increasePriceTenPercent() {

        price += price * 0.1f;
    }
}
