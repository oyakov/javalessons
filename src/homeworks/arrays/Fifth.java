package homeworks.arrays;
//5) В двумерном массиве Вывести максимальный элемент каждой строки
import java.util.Scanner;

public class Fifth {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of rows: ");
        int rows = sc.nextInt();
        System.out.println("Enter the number of columns: ");
        int columns = sc.nextInt();
        int[][] array = new int[rows][columns];

        for (int i = 0; i < rows; i++) {

            int maxElem = 0;

            for (int j = 0; j < columns; j++) {
                array[i][j] = (int) Math.round(Math.random() * 20);

                System.out.print(array[i][j] + "\t");

                if (maxElem < array[i][j]) {
                    maxElem = array[i][j];
                }
            }
            System.out.println("Max = " + maxElem + "\n");
        }
    }
}
