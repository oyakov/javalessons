package homeworks.arrays;
//В двумерном массиве натуральных случайных чисел от 10 до 99 найти количество всех двухзначных чисел,
//   у которых сумма цифр кратная 2.

import java.util.Random;
import java.util.Scanner;

public class Fourth {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the number of rows: ");

        int rows = sc.nextInt();

        System.out.println("Enter the number of columns: ");

        int columns = sc.nextInt();

        int sumMultipleTwo = 0;

        int[][] array = new int[rows][columns];

        Random random = new Random();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {

                array[i][j] = random.nextInt(90) + 10;

                System.out.print(array[i][j] + "\t");

                int dozens = array[i][j] / 10;

                int units = array[i][j] % 10;

                if ((dozens + units) % 2 == 0) {
                    sumMultipleTwo++;
                }
            }

            System.out.println();
        }

        System.out.println("Number of numbers sum is a multiple of two: " + sumMultipleTwo);
    }
}

