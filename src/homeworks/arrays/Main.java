package homeworks.arrays;

public class Main {
    public static final String ANSI_RESET = "\u001B[0m";

    public static final String ANSI_BLUE = "\u001B[34m";

    public static void main(String[] args) {
        Computer[] computers = new Computer[5];

        for (int i = 0; i < computers.length; i++) {
            computers[i] = new Computer(2000, 5000);
            computers[i].view();
        }

        System.out.println(ANSI_BLUE + "Increasing the price by 10%:\n" + ANSI_RESET);

        for (Computer computer : computers) {
            computer.increasePriceTenPercent();
            computer.view();
        }
    }
}
