package homeworks.arrays;

//2) Создать два массива из 30 чисел. Первый массив проинициализировать нечетными числами.
//   Проинициализировать второй массив элементами первого массива при условии,
//   что индекс элемента больше 4 и делится без остатка на 5 и элемент больше 0, но меньше 6 или больше 10, но меньше 20.
//   Если условие не выполняется оставить элемент массива без изменения.
public class Second {
    public static void main(String[] args) {
        int length = 30;

        int[] arr1 = new int[length];//arraySource, arrayTarget

        int[] arr2 = new int[length];

        int odd = -7;

        for (int i = 0; i < length; i++) {
            odd += 2;
            arr1[i] = i * 2 + 1;
            System.out.print(arr1[i] + "\t");
        }

        System.out.println();

        for (int j = 0; j < length; j++) {
            if (j > 4 && j % 5 == 0 && arr1[j] > 0 && (arr1[j] < 6 || arr1[j] > 10) && arr1[j] < 20) {
                arr2[j] = arr1[j];
            }

            System.out.print(arr2[j] + "\t");
        }
    }


}
