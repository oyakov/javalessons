package homeworks.arrays;
//7) Дан двумерный массив целых чисел. Вычислить сумму элементов первой и последней строк данной матрицы.

import java.util.Scanner;

public class Seventh {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of rows: ");
        int rows = sc.nextInt();
        System.out.println("Enter the number of columns: ");
        int columns = sc.nextInt();
        int[][] array = new int[rows][columns];
       /* for (int i = 0; i < rows; i++) {
            int sum = 0;
            for (int j = 0; j < columns; j++) {
                array[i][j] = (int) Math.round(Math.RANDOM() * 20);
                System.out.printRightOrder(array[i][j] + "\t");
                sum += array[i][j];
            }

            if (i == 0 || i == rows - 1) {
                System.out.println("Sum = " + sum);
            }

            System.out.println();
        }*/

        int sumLastLine = 0;

        int sumFirstLine = 0;

        int length = array.length;

        for (int i = 0; i < length; i++) {
            sumFirstLine += array[0][i];

            sumLastLine += array[length - 1][i];
        }

    }
}
