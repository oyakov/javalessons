package homeworks.arrays;

import java.util.Random;
import java.util.Scanner;

//6) В двумерном массиве Необходимо найти и вывести наибольший,
// наименьший элемент, сумму всех элементов каждого столбца.
public class Sixth {

    public static final Scanner SC = new Scanner(System.in);
    public static final Random RANDOM = new Random();

    private static int getSizeFromUser(String message) {
        System.out.println(message);
        return SC.nextInt();
    }

    public static void main(String[] args) {

        int rows = getSizeFromUser("Enter the number of rows: ");

        int columns = getSizeFromUser("Enter the number of columns: ");

        int[][] array = new int[rows][columns];

        int max = 0;
        int min = 0;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                array[i][j] = RANDOM.nextInt(21);

                if (i == 0 && j == 0) {
                    min = array[i][j];
                }

                System.out.print(array[i][j] + "\t");

                if (min >= array[i][j]) {
                    min = array[i][j];
                }

                if (max <= array[i][j]) {
                    max = array[i][j];
                }
            }
            System.out.println();
        }

        sumColumns(array);

        System.out.println("Max = " + max + "\nMin = " + min);
    }

    private static void sumColumns(int[][] sumColumnsArray) {

        int sum;

        int numberColomn = 1;

        for (int i = 0; i < sumColumnsArray[0].length; i++) {
            sum = 0;
            for (int j = 0; j < sumColumnsArray.length; j++) {
                sum += sumColumnsArray[j][i];
            }

            System.out.println("\n" + numberColomn + " Sum = " + sum);

            numberColomn++;

            System.out.println();
        }
    }
}

/*
 * 1 20 3 4 i const, j changes
 * 5 6  0 8
 * 7 8  9 9
 *
 *
 * */

class HomeworkNew {

    public static final Random RANDOM = new Random();

    public static void main(String[] args) {
        int[][] array = new int[4][5];

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = RANDOM.nextInt(40);

                System.out.print(array[i][j] + "\t");
            }

            System.out.println();
        }


        for (int i = 0; i < array[0].length; i++) {

            int sum = 0;

            int max = array[0][i];

            int min = array[0][i];

            for (int j = 0; j < array.length; j++) {
                int element = array[j][i];

                sum += element;

                if (element > max) {
                    max = element;
                }

                if (element < min) {
                    min = element;
                }
            }

            System.out.println("Max of " + i + " column = " + max);
            System.out.println("Min of " + i + " column = " + min);
            System.out.println("Sum of " + i + " column = " + sum);
        }
    }
}

