package homeworks.arrays;
//3) В двумерном массиве целых чисел определить, сколько раз в нем встречается элемент со значением X.

import java.util.Random;
import java.util.Scanner;

public class Third {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the number of rows: ");

        int rows = sc.nextInt();

        System.out.println("Enter the number of columns: ");

        int columns = sc.nextInt();

        System.out.println("Enter the number you are looking for: ");

        int x = sc.nextInt();

        int amountOfNumbers = 0;

        int[][] array = new int[rows][columns];

        Random random = new Random();
//        (max - min + 1) + min)


        /*
        * nextInt(11) + 20
        * */
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                array[i][j] = (int) Math.round(Math.random() * 20);
                array[i][j] = random.nextInt(20);//0 ... 19

                System.out.print(array[i][j] + "\t");

                if (array[i][j] == x) {
                    amountOfNumbers++;
                }
            }
            System.out.println();
        }
        System.out.println("Amount of numbers: " + amountOfNumbers);
    }
}
