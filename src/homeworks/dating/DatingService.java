package homeworks.dating;

import java.util.Objects;
import java.util.Scanner;

public class DatingService {

    private Man[] men;

    public DatingService() {
        this.men = new Man[10];
    }

    public void addNewMan(Man man) {

        if (man.getAge() < 18) {

            System.out.println("Your age should be more than 18!");

            return;
        }

        resize();

        for (int i = 0; i < men.length; i++) {

            if (Objects.isNull(men[i])) {

                men[i] = man;

                break;
            }
        }

        System.out.println("\nSuitable people for " + man.getFirstName() + " " + man.getLastName());

        showPersonsByAgeAndOtherGender(man.getAge(), man.getGender());
    }

    private void resize() {
        int length = men.length;

        if (!Objects.isNull(men[length - 1])) {

            Man[] myArray = new Man[length * 2];

            for (int i = 0; i < men.length * 2; i++) {

                myArray[i] = men[i];
            }

            men = myArray;
        }
    }

    private void showPersonsByAgeAndOtherGender(int age, Gender gender) {
        for (int i = 0; i < men.length; i++) {

            if (!Objects.isNull(men[i]) &&
                    (men[i].getGender() != gender) && ((age - 3) <= men[i].getAge()) && (men[i].getAge() <= (age + 3))) {

                men[i].printFields();
            }
        }
    }

    public void showPersonsByGender(Gender gender) {
        for (int i = 0; i < men.length; i++) {

            if (!Objects.isNull(men[i]) && men[i].getGender() != gender) {

                System.out.println(men[i].getFirstName() + " " + men[i].getLastName() + " "
                        + men[i].getCity() + " " + men[i].getAge() + " "
                        + men[i].getNumberOfChildren() + " " + men[i].getGender());
            }
        }
    }

    public void searchByFirstNameAndLastName(String firstName, String lastName) {
        for (int i = 0; i < men.length; i++) {

            if (!Objects.isNull(men[i])) {

                if (men[i].getFirstName().equals(firstName) && men[i].getLastName().equals(lastName)) {

                    System.out.println(men[i].getFirstName() + " " + men[i].getLastName() + " "
                            + men[i].getCity() + " " + men[i].getAge() + " "
                            + men[i].getNumberOfChildren() + " " + men[i].getGender());
                }
            }
        }
    }

    public void smartSearch(String city, int theYoungestAge, int theEldestAge, int minNumberOfChildren,
                            int maxNumberOfChildren, Gender gender) {
        for (int i = 0; i < men.length; i++) {//foreach

            if (!Objects.isNull(men[i])) {

                if (men[i].getFirstName().equals(city) && men[i].getGender() == gender
                        && theYoungestAge <= men[i].getAge() && men[i].getAge() <= theEldestAge
                        && minNumberOfChildren <= men[i].getNumberOfChildren()
                        && men[i].getNumberOfChildren() <= maxNumberOfChildren) {

                    System.out.println(men[i].getFirstName() + " " + men[i].getLastName() + " "
                            + men[i].getCity() + " " + men[i].getAge() + " "
                            + men[i].getNumberOfChildren() + " " + men[i].getGender());//printFields
                }
            }
        }
    }
}
