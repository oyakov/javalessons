package homeworks.dating;

public class Man {
    private String firstName;

    private String lastName;

    private String city;

    private int age;

    private int numberOfChildren;

    private Gender gender;

    public Man(String firstName, String lastName, String city,
               int age, int numberOfChildren, Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.age = age;
        this.numberOfChildren = numberOfChildren;
        this.gender = gender;
    }

    public Gender getGender() {
        return gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    public int getNumberOfChildren() {
        return numberOfChildren;
    }

    public void printFields() {
        /*System.out.println(firstName + " " + men[i].getLastName() + " "
                + men[i].getCity() + " " + men[i].getAge() + " "
                + men[i].getNumberOfChildren() + " " + men[i].getGender());*/
    }

}

enum Gender {
    MALE("Male"), FEMALE("Female");

    private String shortName;

    Gender(String gender) {
        this.shortName = gender;
    }

    public String getShortName() {
        return shortName;
    }


}
