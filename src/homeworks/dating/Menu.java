package homeworks.dating;

import java.util.Scanner;

public class Menu {
    private static final Scanner SC = new Scanner(System.in);

    public static void main(String[] args) {

        DatingService service = new DatingService();

        Man men1 = new Man("Olesia", "Yakovenko", "Vinnitsya", 18, 0, Gender.FEMALE);

        Man men2 = new Man("Vlad", "Filippenko", "Kaliningrad", 19, 0, Gender.MALE);

        Man men3 = new Man("Alan", "Chen", "Kyiv", 21, 3, Gender.MALE);

        while (true) {
            menu();

            int key = SC.nextInt();

            try {
                switch (key) {
                    case 1: {
                        String firstName = getInfoFromUser("Enter your first name: ");

                        String lastName = getInfoFromUser("Enter your last name: ");

                        String city = getInfoFromUser("Enter your city: ");

                        int age = getNumberFromUser("Enter your age: ");

                        int numberOfChildren = getNumberFromUser("Enter your number of children: ");

                        Gender gender = Gender.valueOf(getInfoFromUser("Enter your gender: ").toUpperCase());

                        Man man = new Man(firstName, lastName, city, age, numberOfChildren, gender);

                        service.addNewMan(man);

                        break;
                    }
                    case 2: {
                        Gender gender = Gender.valueOf(getInfoFromUser("Enter your gender: ").toUpperCase());
                        service.showPersonsByGender(gender);
                        break;
                    }
                    case 3: {
                        String firstName = getInfoFromUser("Enter first name: ");

                        String lastName = getInfoFromUser("Enter last name: ");

                        service.searchByFirstNameAndLastName(firstName, lastName);

                        break;
                    }

                    case 4: {
                        String city = getInfoFromUser("Enter city: ");

                        int theYoungestAge = getNumberFromUser("Enter the youngest age: ");

                        int theEldestAge = getNumberFromUser("Enter the eldest age: ");

                        int theSmallestNumberOfChildren = getNumberFromUser("Enter the smallest number of children: ");

                        int theLargestNumberOfChildren = getNumberFromUser("Enter the largest number of children: ");

                        Gender gender = Gender.valueOf(getInfoFromUser("Enter gender: ").toUpperCase());

                        service.smartSearch(city, theYoungestAge, theEldestAge,
                                theSmallestNumberOfChildren, theLargestNumberOfChildren, gender);

                        break;
                    }

                    default:
                        return;
                }
            } catch (Exception e) {
                System.out.println("Exception" + e);
            }
        }
    }


    private static String getInfoFromUser(String message) {
        System.out.println(message);
        return SC.next();
    }

    private static int getNumberFromUser(String message) {
        System.out.println(message);
        return SC.nextInt();
    }

    public static void menu() {
        System.out.println("1) Sign up.\n" +
                "2) Print a list of suitable people for you by gender.\n" +
                "3) View profile of person.\n" +
                "4) Search.\n" +
                "5) Exit.");
    }
}

