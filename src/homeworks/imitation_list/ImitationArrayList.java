package homeworks.imitation_list;

public class ImitationArrayList {

    private int[] array;

    public ImitationArrayList(int size) {

        this.array = new int[size];//0 0 0 0
    }

    //TODO only for unit tests
    public int[] getArray() {
        return array;
    }

    //TODO only for unit tests
    public void setArray(int[] array) {
        this.array = array;
    }

    public void addElement(int element) {

        resize();

        for (int i = 0; i < array.length; i++) {

            if (array[i] == 0) {

                array[i] = element;

                break;
            }
        }

    }

    private void resize() {
        int length = array.length;

        if (array[length - 1] != 0) {
            increaseArray(length * 2);
        }
    }

    public void printRightOrder() {//foreach !

        for (int arrayPrint : array) {

            System.out.print(arrayPrint + "\t");
        }

        System.out.println();
    }

    public void printReverseOrder() {

        for (int i = array.length - 1; i >= 0; i--) {

            System.out.print(array[i] + "\t");
        }

        System.out.println();
    }

    public void replaceElementByIndex(int searchKey, int newNumber) {//add checking for existing index

       /* for (int i = 0; i < array.length; i++) {

            if (i == searchKey - 1) {

                array[i] = newNumber;
            }

        }*/

        array[searchKey] = newNumber;

        printRightOrder();
    }


    public void removeElementByIndex(int index) {

        int[] myArray = new int[array.length - 1];

        for (int i = 0; i < index - 1; i++) {
            myArray[i] = array[i];
        }

        for (int i = index - 1; i < array.length - 1; i++) {
            myArray[i] = array[i + 1];
        }

        array = myArray;
    }

    public void increaseArray(int numberOfElements) {

        int[] myArray = new int[array.length + numberOfElements];

        for (int i = 0; i < array.length; i++) {

            myArray[i] = array[i];
        }

        array = myArray;
    }

    public void reduceArray(int numberOfElements) {

        int[] myArray = new int[array.length - numberOfElements];

        for (int i = 0; i < array.length - numberOfElements; i++) {

            myArray[i] = array[i];
        }

        array = myArray;

        printRightOrder();
    }

    public void bubbleSort() {

        for (int i = array.length - 1; i > 0; i--) {

            for (int j = 0; j < i; j++) {

                if (array[j] > array[j + 1]) {

                    int tmp = array[j];

                    array[j] = array[j + 1];

                    array[j + 1] = tmp;
                }
            }
        }

        printRightOrder();
    }

    public void addArrayToArray(int[] newArray) { //int[] newArray 5

        int zero = 0;//3

        int length = newArray.length;

        for (int i = 0; i < array.length; i++) {//4

            if (array[i] == 0) {

                zero++;
            }
        }

        int[] myArray = new int[array.length + length - zero];//4 + 5 - 3 = 6

        for (int i = 0; i < array.length - zero; i++) {//1

            myArray[i] = array[i];

        }

        for (int i = 0; i < length; i++) {//5

            myArray[i + array.length - zero] = newArray[i];
        }

        array = myArray;

        printRightOrder();
    }

    public void addArrayToArrayNew(int[] newArray) { //int[] newArray 5

        int countZero = 0;//3

        /*
         * 1 2 3 4 0 0  + 7 8 9
         * */

        int length = newArray.length;

        int positionFirstZero = array.length - countZero;

        for (int i : array) {//4

            if (i == 0) {

                countZero++;
            }
        }

        if (countZero < newArray.length) {
            increaseArray(newArray.length - countZero + 1);
        }

        for (int i = positionFirstZero, j = 0; j < length; ++i, ++j) {
            array[i] = newArray[j];
        }

        printRightOrder();
    }


    public void removeDuplicates() {

        for (int i = 0; i < array.length; i++) {//1 1 2 3 5 5 1 -> 1 2 3 5 5

            for (int j = i + 1; j < array.length; j++) {

                if (array[j] == array[i]) {

                    removeElementByIndex(j + 1);

                    --j;
                }
            }
        }

        printRightOrder();
    }

    public void linearSearch(int number) {

        for (int i = 0; i < array.length; i++) {

            if (array[i] == number) {

                System.out.println("Number " + number + "\tPlace: " + (i + 1));
            }
        }
    }
}

//resize


