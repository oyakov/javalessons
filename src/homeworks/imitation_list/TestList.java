package homeworks.imitation_list;

public class TestList {
    public static void main(String[] args) {
        ImitationArrayList numbers = new ImitationArrayList(5);

        int[] myArray = {1, 1, 5, 7, 10, 33};

        for (int i = 1; i < 6; i++) {

            numbers.addElement(i);
        }

        numbers.printRightOrder();

       /* numbers.printReverseOrder();

        numbers.replaceElementByIndex(3, 5);

        numbers.removeElementByIndex(3);

        numbers.printRightOrder();

        numbers.increaseArray(3);

        numbers.printRightOrder();

        numbers.reduceArray(2);

        numbers.bubbleSort();

        numbers.addArrayToArray(myArray);

        numbers.removeDuplicates();

        numbers.linearSearch(2);*/

        numbers.addArrayToArrayNew(myArray);

    }
}
