package lessons.first_lesson.for_test;

import java.util.Random;

public class Calculator {
    public int sum(int value1, int value2) {
        return value1 + value2;
    }

    public int minus(int value1, int value2) {
        return value1 - getRandomNumber();
    }

    public int getRandomNumber() {
        return new Random().nextInt(20);
    }
}