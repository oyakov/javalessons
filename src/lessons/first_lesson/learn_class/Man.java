package lessons.first_lesson.learn_class;

public class Man {
    private int age;//0

    private Man man;

    private String name;//null

    private static String firstName = "Static variable";

    public static final int COUNT;

    public final int LENGTH = 5;

    {
//        LENGTH = 5;
        System.out.println("Non-static block initialization");
    }

    static {
        COUNT = 45;
        System.out.println("Hello world");
    }

    public Man(int age1) {
//        LENGTH = 5;

        System.out.println("Constructor");
        age = age1;
    }

    public Man(int age, String name) {

        this(name, age);

//        LENGTH = 5;

    }

    public Man(String name, int age) {
        this.age = age;
        this.name = name;
    }

    public static String getFirstName() {

        return firstName;
    }

    public static void setFirstName(String firstName) {
        Man.firstName = firstName;
    }

    public void setAge(int age) {//this

        int run = 2;

        if (age < 0) {
            int r = 0;
            System.out.println("You're wrong");
            return;
        }

        System.out.println(run);
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public static void main(String[] args) {

    }
}

class TestMan {
    public static void main(String[] args) {
        /*type_reference name_reference = new type_reference(incoming parameters or absent)*/
        Man man = new Man(10);

        man = new Man(45);

        man.setAge(-100);
    }
}
