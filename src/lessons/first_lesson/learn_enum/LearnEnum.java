package lessons.first_lesson.learn_enum;

public class LearnEnum {

    public static void print(String gender) {//Male, Female

    }

    public static void print(Sex gender) {//Male, Female

    }


    public static void main(String[] args) {
        Sex male = Sex.MALE;

        String sex = "MaLe";

        Sex sex1 = Sex.valueOf(sex.toUpperCase());

        for (Sex sex2 : Sex.values()) {
            System.out.println(sex2);
        }

//        System.out.println(male.getShortName());
    }
}

enum Sex {
    MALE("Male"), FEMALE("Female");

    private String shortName;

    Sex(String sex) {
        this.shortName = sex;
    }

    public String getShortName() {
        return shortName;
    }
}
