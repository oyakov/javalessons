package lessons.first_lesson.learn_string;

public class LearnString {
    public static void main(String[] args) {
        String str = "Hello";//object-literal

        String str1 = new String("Hello");//object

//        System.out.println("str == str1: " + (str.equals(str1)));
//        System.out.println("str == str1: " + (str == str1.intern()));

//        System.out.println("str == str1: " + (str == str1));

//        String x = "1" + 2 + 3;

//        System.out.println(x);//123

//        System.out.println(1 + 2 + "3");//33

        String x = str.toLowerCase();

//        System.out.println(x);
//        System.out.println(str.charAt(0));

        String x1 = str.substring(0, 2) + "m" + str.substring(3);

        System.out.println(str);
    }
}
