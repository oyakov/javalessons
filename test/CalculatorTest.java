import lessons.first_lesson.for_test.Calculator;
import org.junit.*;

public class CalculatorTest {

    @Before
    public void beforeEachTest() {
        System.out.println("Before each test");
    }

    @BeforeClass
    public static void beforeAllTests() {
        System.out.println("Before all tests");
    }

    @AfterClass
    public static void afterAllTests() {
        System.out.println("After all tests");
    }

//    @Ignore
    @Test
    public void shouldReturnSumValues() {
        Calculator calculator = new Calculator();

        Assert.assertEquals("Should return 10", 10, calculator.sum(6, 4));
    }

    @Test
    public void shouldReturnSumValues1() {
        Calculator calculator = new Calculator();

        Assert.assertEquals("Should return 10", 10, calculator.sum(6, 4));
    }

    @After
    public void afterEachTest() {
        System.out.println("After each test");
    }
}
