import homeworks.imitation_list.ImitationArrayList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ImitationListTest {
    private ImitationArrayList list = new ImitationArrayList(10);

    @Before
    public void cleanArray() {
        list.setArray(new int[3]);//5 0 0
    }

    @Test
    public void shouldAddElement() {
        list.addElement(5);
        list.addElement(2);

        int[] array = list.getArray();

        Assert.assertEquals(5, array[0]);

        Assert.assertEquals(2, array[1]);
    }

    @Test
    public void shouldChangeElementByIndex() {
        list.addElement(5);

        list.replaceElementByIndex(0, 8);

        int[] array = list.getArray();

        Assert.assertEquals(8, array[0]);

    }

}
